# main.tf
provider "kubernetes" {
  config_path = "/var/lib/jenkins/.kube/config"
}

resource "kubernetes_namespace" "npmaven" {
  metadata {
    name = "npmaven"
  }
}

resource "kubernetes_deployment" "mvn-deployment" {
  metadata {
    name = "mvn-deployment"
    namespace = kubernetes_namespace.npmaven.metadata[0].name
  }

  spec {
    replicas = 2

    selector {
      match_labels = {
        app = "maven-app"
      }
    }

    template {
      metadata {
        labels = {
          app = "maven-app"
        }
      }

      spec {
        container {
          image = "gayeline/multistage:v1"
          name  = "maven"

          port {
            container_port = 2222
          }
        }
      }
    }
  }
}

resource "kubernetes_service" "srvmaven" {
  metadata {
    name = "srvmaven"
    namespace = kubernetes_namespace.npmaven.metadata[0].name
  }

  spec {
    selector = {
      app = kubernetes_deployment.mvn-deployment.spec[0].template[0].metadata[0].labels.app
    }

    port {
      port        = 2222
      target_port = 2222
    }

    type = "NodePort"
  }
}